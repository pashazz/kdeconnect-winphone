﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Windows.Networking;
using Windows.Networking.Sockets;
using Windows.Storage.Streams;
using Org.BouncyCastle.Crypto;
using KdeConnectEarly.Core;
using KdeConnectEarly.Core.Backends.Lan;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;

using Org.BouncyCastle.Asn1.Cmp;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Security.Certificates;


namespace TestKdeConnect
{
    [TestClass]
    public class UnitTest1
    {

        /// <summary>
        /// If this test will fail with protocolversion, 5: Ignore. 
        /// This is Json.net issue with long vs int (that) won't be fixed
        /// by them
        /// </summary>
        [TestMethod]
        public void TestEncryption()
        {
            //Get private key
            RsaPrivateCrtKeyParameters priv = Crypto.getPrivateKeyFromString
                #region long key

                (@"-----BEGIN PRIVATE KEY-----
MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDDBFTYTokn2hig
LxVWBCQkdQXGuNvOVH/LFAVOiHqd6HO4SDM9SVeiNTAhL+Pc2mjL5XQzZClBrcXe
58N1sEC0rgjgDQ/9Tme+l5Xmrf1q+iBgOLYBDFyyMkr8+3IhTITzSG2nqCWQ15dZ
Wm4DN6OBkcvvwrTzPMBR6bZGWU7PE7cR5RxkvLB9mvm++Lnvs3+QUU48qhBatnmg
xYqXmt1vKA1XqBbsFpDwhM1Ee6HCNaAzWTUT1gE8d5qU5SJ5H0M3DTkGSuM+yqu8
ll/CN4Gn8C00ZDvBEe0KpWSOoCFSWq2wpT1zY0oPu4P40WZn+yCs0sc7VxMaAMIG
q5V271mxAgMBAAECggEAe8Il7dd5Jvc7JXx7t8iM/y3cao4pv7jUGJYt//aQieFY
fpwjGEOQ/3ltf8Iagq+ATdThdzObEvkqVxkRa4Bcr8y6CSWA/ilhoQwlruy/o8H0
Bdw3dFbaBKPx/rW7afJbdOCD5h/JJzAJ9kRzCEuo0bMqjo8rsijdi54bdG18m+A1
CBV5VEhppvmVgIReuzer2z03RkdmJqH6OADsSKHK3r21g2edG3KK0XTILLyop/bm
ArETOgImIM3WN6WgC6rxZ6QfGjJuPPYeK5+amPkTf0jSw/j5Yb4EgHclPri4cQz2
eZx8XCYmDxJQBGViYFviUJhWywuel7UcRpaOuI6ImQKBgQDsLdqDHI+EaWscKLzb
21dYrRtxX3AKoBKB6/JxUUMT+PFeO1G012R8h/oVaG7fzB9R9lT2cBXOhniYSRUt
SzEJqMrvu3GOZ18nN0bFFtACBcp46hW0GPFrkBmaJXDkHnO/Olv0QTURP1Ojlfzu
LuPDPwCEoyx2WRkGejcujD+2VwKBgQDTYiLkK0wY0QMv8cn5Vn2E/Av0QexzSAcN
AP7yj34KCkhV/9TSmyFOCNdg/6KCFvvjGeyuvv+CzFPvEfDg2LjzNYOtCARtprRd
eRNXmPotVZ9O/kpDGCUx02gRIupg4WMvkc6V+UDCr5VSG4SeM/TEk7WhG2hjr3zK
QwEUgJwbNwKBgQDbYrigo84YtIffT2QnYfcgEyTzyjBcXlbywoahhlel3M/wtW9v
ackY+IJwkmKzlnvdedFwXauT/UtWQkTenwL9MwLXb6nLGvDDj9A4UbmEt3ZdnSvt
8GGJhgCSNIdKD5N7Ja23FT9DNyztPu0FHx9JBWPo0V4CzT62yp4pqfOg3wKBgCL0
+bwuBNmdxx8opzQav4s8e2WbeVTfNntKDN9kJ33VkXzXYASsLufOwn+CBwfocSNv
aBrF709cJw1ENEeaz4s48FuvFbxkczN6EF7OJ6FTpZLUrsdFuPDyeyVKVuRKobJ+
NGHM5I7Npq37awUWpoXTv5ezBqJlpQd16TnQwwAlAoGAY8UmLLGM17lWQvjAeKIr
p+3GAy2cZNsgyTFmtkGA4AYaIrDSof5AosfbK0HIE5tSGi4VASnxjLv9SnTHOrZc
W2VWNB1X7f3N9ePZFi5xSW7K5ZGNIslfUiwucC6gledIiHZIkr8CU+Az2ovp/u4z
GDRiIxRLjxNvEoDQxGhUt7I=
-----END PRIVATE KEY-----"

                #endregion

                );
            RsaKeyParameters pub = Crypto.getPublicKeyByPrivate(priv);
            NetworkPackage pkg = NetworkPackage.createIdentityPackage();
            NetworkPackage encrypted =
                NetworkPackage.encrypt(pkg, pub);

            NetworkPackage decrypted = NetworkPackage.decrypt(encrypted, priv);
            Assert.AreEqual(pkg.id, decrypted.id);
            Assert.AreEqual(pkg.payloadSize, decrypted.payloadSize);
            Assert.AreEqual(pkg.payloadTransferInfo, decrypted.payloadTransferInfo);
            Assert.AreEqual(pkg.type, decrypted.type);
            CollectionAssert.AreEquivalent(pkg.body, decrypted.body);

        }


        [TestMethod]
        public void TestIdentity()
        {
            NetworkPackage pkg =
                NetworkPackage.createIdentityPackage();
            Debug.WriteLine("Test successful!");
            Assert.AreEqual(pkg.type, PackageTypes.PACKAGE_TYPE_IDENTITY);

        }
        /// <summary>
        /// THIS TEST SHOULD GENERATE EXCEPTION CATCHED BY  CONSUMESTREAM ITSELF!!!
        /// SEE DEBUG!
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task TestSocket()
        {
            //Create TCP listener
            int port = 1789;
            byte[] messageToSend = Encoding.UTF8.GetBytes("Hello\nHello\nHello\n"); //broken
            StreamSocketListener listener = new StreamSocketListener();
            int calls = 0;
            listener.ConnectionReceived += async (sender, args) =>
            {
                
                var reader = new TcpSocketConsumer(args.Socket);
                reader.messageReceived += (o, eventArgs) =>
                {
                    CollectionAssert.AreEquivalent(eventArgs.data, Encoding.UTF8.GetBytes("Hello\n"));
                    calls++;
                };
                reader.error += (o, eventArgs) =>
                {
                    if (eventArgs.status != 0)
                        Assert.Fail(); //Error
                };
                await reader.consumeStream();
            };
            //Bind
            await listener.BindServiceNameAsync(port.ToString());
            //Connect
            StreamSocket feeder = new StreamSocket();
            await feeder.ConnectAsync(new HostName("127.0.0.1"), port.ToString());
            //Write
            DataWriter write = new DataWriter(feeder.OutputStream);
            write.WriteBytes(messageToSend);
            await write.StoreAsync();
            await Task.Delay(100);
            write.WriteBytes(messageToSend);
            await write.StoreAsync();
            await Task.Delay(100);
            //write.Dispose();
            Assert.AreEqual(6, calls);
        }

    }
}

