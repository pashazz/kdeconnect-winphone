﻿namespace KdeConnectEarly
{
    public class Constants
    {
        public const byte MessageSeparator = 0xA; // \n
        public const string DevicesFolder = "Devices"; //Used by device class
        public const int ProtocolVersion = 5; //NetworkPackage protocol version

    }
}