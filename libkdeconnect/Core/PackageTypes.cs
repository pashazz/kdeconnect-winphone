﻿namespace KdeConnectEarly.Core
{
    public partial class PackageTypes
    {
        /// <summary>
        /// Identity package
        /// <see cref="NetworkPackage.createIdentityPackage"/>
        /// </summary>
        public const string PACKAGE_TYPE_IDENTITY = "kdeconnect.identity";
        public const string PACKAGE_TYPE_PAIR = "kdeconnect.pair";
        public const string PACKAGE_TYPE_ENCRYPTED = "kdeconnect.encrypted";
       
    }
}