﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.IO.IsolatedStorage;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Windows.ApplicationModel.Core;
using Windows.Storage;
using Newtonsoft.Json;
using Org.BouncyCastle.Crypto.Parameters;
using KdeConnectEarly.Core.Backends;

// ReSharper disable ExplicitCallerInfoArgument

namespace KdeConnectEarly.Core
{
    /// <summary>
    /// Reasons why pairing to this device is failed
    /// </summary>
    public class DevicePairingFailedEventArgs : EventArgs
    {
        public enum Reason
        {
            Paired, AlreadyRequested, NotReachable, ConnectionClosed, RejectedByPeer, Rejected
        }

        public Reason reason {get; set; }
    }
  
    /// <summary>
    /// Represents physical devices identified by unique string
    /// If you want to get a device that is already known, use  loadTrustedDevice
    /// If 
    /// If you want to be subscribed for incoming packages (except on ones for pairing), go packageReceived
    /// 
    /// </summary>
    public sealed class Device : INotifyPropertyChanged, IDisposable
    {
        #region Enums
        public enum DeviceType
        {
            Desktop,
            Laptop,
            Phone,
            Tablet,
            Unknown
        }

        public enum PairStatus
        {
            NotPaired,
            Requested,
            RequestedByPeer,
            Paired
        }
        #endregion
        #region Enum => string
        public class DeviceTypeConverter
        {
            public static string deviceToString(DeviceType value)
            {
                switch (value)
                {
                   case DeviceType.Desktop:
                        return "desktop";
                   case DeviceType.Laptop:
                        return "laptop";
                   case DeviceType.Phone:
                        return "phone";
                    case DeviceType.Tablet:
                        return "tablet";
                    default:
                       return "unknown";
                }                
            }

            public static DeviceType stringToDevice(string s)
            {
                DeviceType d;
                if (s == "desktop")
                    d = DeviceType.Desktop;
                else if (s == "laptop")
                    d = DeviceType.Laptop;
                else if (s == "phone")
                    d = DeviceType.Phone;
                else if (s == "tablet")
                    d = DeviceType.Tablet;
                else if (s == "unknown")
                    d = DeviceType.Unknown;
                else
                {
                    throw new ArgumentOutOfRangeException("s");
                }
                return d;
            }
        }
        #endregion

        [DataContract]
        public class TrustedDevice
        {
            /// <summary>
            /// Constructor is called before actual deserializing so we can set defaults
            /// </summary>
            public TrustedDevice()
            {
                deviceName = "unnamed";
                deviceType = "unknown";
            }
            [DataMember]
            public string  deviceName { get; set; }

            [DataMember]
            public string deviceType { get; set; }
         
        }


        /// <summary>
        /// Use this constructor to create already known and trusted device (saved on disk)
        /// </summary>
        /// <param name="id"></param>
        public static async Task<Device> loadTrustedDevice(string id)
        {
            using (var store = IsolatedStorageFile.GetUserStoreForApplication())
            {
                TrustedDevice device;

                using (var file = store.OpenFile(System.IO.Path.Combine
                    (Constants.DevicesFolder, id), FileMode.Open))
                {
                    using (var reader = new StreamReader(file))
                    {
                        device = JsonConvert.DeserializeObject<TrustedDevice>(await reader.ReadToEndAsync());
                    }
                }
                //Load RsaKeyParameters from PEM file
                RsaKeyParameters pubKey;
                using (var file = store.OpenFile(Path.Combine(Constants.DevicesFolder, id + ".pem"), FileMode.Open))
                {
                    using (var reader = new StreamReader(file))
                    {
                        pubKey = Crypto.getPublicKeyFromReader(reader);
                    }
                }
                Debug.WriteLine("Device: new trusted device {0} has been loaded", id);
                return new Device(id, device.deviceName,
                    DeviceTypeConverter.stringToDevice(device.deviceType), pubKey);
            }
        }

        private Device(string id, string name, DeviceType type, RsaKeyParameters pubKey)
        {
            m_pairStatus = PairStatus.Paired;
            this.id = id;
            m_pubKey = pubKey;
            m_deviceType = type;
            m_privateKey =
         (RsaPrivateCrtKeyParameters)(CoreApplication.Properties["privateKey"]);
            this.name = name;

            initializeLinksArray();
        }

        private void initializeLinksArray()
        {
            m_links = new ObservableCollection<Link>();
            m_links.CollectionChanged += (sender, args) =>
            {
                if (args.Action == NotifyCollectionChangedAction.Add && m_links.Count == 1)
                {//First link has been added
                    Debug.WriteLine("Device {0} is reachable now!", this.name);
                    onPropertyChanged("reachable");
                }
                else if (m_links.Count == 0)
                {
                    Debug.WriteLine("Device {0} is unreachable now!", this.name);
                    onPropertyChanged("reachable");
                }
            };
        }

        /// <summary>
        /// use this constructor to initialise new devices
        /// </summary>
        /// <param name="identity">Identity package</param>
        /// <param name="link">Device link</param>
        public Device(NetworkPackage identity, Link link)
        {
            id = identity.get("deviceId", "");
            name = identity.get("deviceName", "");
            m_deviceType = DeviceTypeConverter.stringToDevice(identity.get("type", "unknown"));
            pairStatus = PairStatus.NotPaired;
            m_protocolVersion = identity.getInt("protocolVersion", Constants.ProtocolVersion);
            //TODO incoming/outgoing interfaces
            m_privateKey = (RsaPrivateCrtKeyParameters) CoreApplication.Properties["privateKey"];

            initializeLinksArray();
            addLink(identity, link);
        }

        private IsolatedStorageFile store = IsolatedStorageFile.GetUserStoreForApplication();
        private RsaKeyParameters m_pubKey;
        private RsaPrivateCrtKeyParameters m_privateKey; //My private key
        private int m_protocolVersion;
        private DeviceType m_deviceType;
        private bool m_needsSerializing =  false; //Change this to true when name or type has changed during lifetime of object. Then serialize on dispose
        private PairStatus m_pairStatus;
        private List<string> m_incomingCapabilities; //TODO
        private List<string> m_outgoingCapabilities; //TODO
        private ObservableCollection<Link> m_links;

        public PairStatus pairStatus
        {
            get
            {
                return m_pairStatus;
            }
            private set
            {
                m_pairStatus = value;
                onPropertyChanged();
            }
        }

        public bool reachable { get { return m_links.Count != 0; } }
        public string id { get; private set; }
        public string name { get; private set; }

        public event EventHandler<DevicePairingFailedEventArgs> pairingFailed;
        public event PackageReceivedHandler packageReceived;
        public event PropertyChangedEventHandler PropertyChanged;

   
        private void onPairingFailed(DevicePairingFailedEventArgs args)
        {
            if (pairingFailed != null)
                pairingFailed(this, args);
        }
        
        /// <summary>
        /// Call when pairing is succeeded
        /// </summary>
        /// <returns></returns>
        private async Task onPairingSucceeded()
        {
            pairStatus = PairStatus.Paired;
            await serialize();
            using (var writer = new StreamWriter(store.OpenFile(
                    Path.Combine(Constants.DevicesFolder, id + ".pem"), FileMode.CreateNew)))
            {
                    Crypto.writeKey(writer, m_pubKey);
                    await writer.FlushAsync();
            }
            Debug.WriteLine("pairing succeeded");

        }


        /// <summary>
        /// User on this device removed us from trusted. OK then, I'll remove them too!
        /// </summary>
        /// <returns></returns>
        private void onRemoved()
        {
            pairStatus = PairStatus.NotPaired;
            store.DeleteFile(Path.Combine(Constants.DevicesFolder, id));
            store.DeleteFile(Path.Combine(Constants.DevicesFolder, id + ".pem"));

        }
  

        private void onPropertyChanged([CallerMemberName] string propName = "")
        {
            if (propName == "name" || propName == "type")
                m_needsSerializing = true;

            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }



        public async Task requestPair()
        {
            switch (pairStatus)
            {
                    case PairStatus.Paired:
                        onPairingFailed(new DevicePairingFailedEventArgs()
                        {reason = DevicePairingFailedEventArgs.Reason.Paired});
                        return;
                    case PairStatus.Requested:
                        onPairingFailed(new DevicePairingFailedEventArgs()
                        {reason = DevicePairingFailedEventArgs.Reason.AlreadyRequested});
                        return;
                    default:
                    if (!reachable)
                    {
                        onPairingFailed(new DevicePairingFailedEventArgs()
                        {reason = DevicePairingFailedEventArgs.Reason.NotReachable});
                        return;
                    }
                    break;
            }

            pairStatus = PairStatus.Requested;
            

            bool success = await sendMyPublicKey();
            if (!success)
            {
                pairStatus = PairStatus.NotPaired;
                onPairingFailed(new DevicePairingFailedEventArgs()
                    {reason = DevicePairingFailedEventArgs.Reason.ConnectionClosed});
                return;
            }
            //wait for answer
            //See method onPackageReceived which does actual pairing
        }

        private async Task<bool> sendMyPublicKey()
        {
            NetworkPackage package = new NetworkPackage(PackageTypes.PACKAGE_TYPE_PAIR);
            package.set("pair", true);
   

            var pubKey = Crypto.getPublicKeyByPrivate(m_privateKey);
            return await sendPackage(package);
        }

        private async Task<bool> sendPackage(NetworkPackage package)
        {
            if (package.type != PackageTypes.PACKAGE_TYPE_PAIR && pairStatus == PairStatus.Paired)
            {//Send everything encrypted
                foreach (Link link in m_links)
                {
                    if (await link.sendEncryptedPackage(package, m_pubKey))
                    {
                        Debug.WriteLine("Device {0}: package sent encrypted over Link of type {1}",
                            id, link.provider.name);
                        return true;
                    }
                }
            }
            else
            {
                foreach (Link link in m_links)
                    if (await link.sendPackage(package))
                    {
                        Debug.WriteLine("Device {0}: package sent successfully over Link of type {1}",
                            id, link.provider.name);

                        return true;
                    }
            }
            return false;
        }

        public async Task rejectPairing()
        {
          
            //Send rejection package
            NetworkPackage package = new NetworkPackage(PackageTypes.PACKAGE_TYPE_PAIR);
            package.set("pair", false);
            await sendPackage(package);
            pairStatus = PairStatus.NotPaired;
            if (pairStatus == PairStatus.RequestedByPeer)
                onPairingFailed(new DevicePairingFailedEventArgs()
                    {reason = DevicePairingFailedEventArgs.Reason.Rejected});

        }

        public async Task acceptPairing()
        {
            if (pairStatus != PairStatus.RequestedByPeer)
            {
                Debug.WriteLine("Warning: call acceptPairing with wrong PairStatus: {0}", pairStatus);
                return;
            }
            Debug.WriteLine("Pairing to {0} is accepted", id);

            if (!await sendMyPublicKey())
            {
                pairStatus = PairStatus.NotPaired;
                onPairingFailed(new DevicePairingFailedEventArgs()
                    {reason = DevicePairingFailedEventArgs.Reason.ConnectionClosed});
            }
            await onPairingSucceeded();
        }

        public void addLink(NetworkPackage identity, Link link)
        {
            m_protocolVersion = identity.getInt("protocolVersion", Constants.ProtocolVersion);
            if (m_protocolVersion != Constants.ProtocolVersion)
            {
                Debug.WriteLine("Warning: Link to device {0} uses protocol version {1}, which we do not support",
                    id, m_protocolVersion);
            }
            //Connect to Link events
            link.packageReceived += onPackageReceived;
            link.linkLost += onLinkLost;
            //No plugin stuff YET
            //TODO initialize plugins

            m_links.Add(link);
            //Check if we need to change name and/or type
            if (name != identity.get("deviceName", name))
            {
                name = identity.get("deviceName", name);
                onPropertyChanged("name");
            }

        }

        /// <summary>
        /// This method only deals with PAIR packages. Other packages go directly to plugins
        /// who should subscribe to device's packageReceived
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="package"></param>
        private async void onPackageReceived(object sender, NetworkPackage package)
        {
            Debug.WriteLine("Device::onPackageReceived: Received a package through {0} for device {1}",  sender.GetType(), name);
            if (package.type != PackageTypes.PACKAGE_TYPE_PAIR)
            {
                Debug.WriteLine("Ignoring package - firing event for plugins");
                if (packageReceived == null)
                {
                    Debug.WriteLine("WARNING: NO PLUGINS REGISTERED");
                    return;
                }
                packageReceived(this, package);
            }
            else
            {//Pairing process
                bool wantsPair = package.getBool("pair", false);
                if (wantsPair && pairStatus == PairStatus.Paired)
                {
                    Debug.WriteLine("Device '{0}' had already been paired, but sent another pair package", name);
                    return;
                }
                else if (!wantsPair && pairStatus == PairStatus.Requested)
                {
                    Debug.WriteLine("Pair request was cancelled by user on the device {0}", name);
                    pairStatus = PairStatus.NotPaired;
                    onPairingFailed(new DevicePairingFailedEventArgs()
                    {reason = DevicePairingFailedEventArgs.Reason.RejectedByPeer});
                    return;
                }
                else if (!wantsPair && pairStatus == PairStatus.RequestedByPeer)
                {
                    Debug.WriteLine("User on the device '{0}' has changed their mind on pairing and rejected", name);
                    onPairingFailed(new DevicePairingFailedEventArgs()
                        {reason = DevicePairingFailedEventArgs.Reason.RejectedByPeer});
                }
                else if (wantsPair && pairStatus == PairStatus.Requested)
                {
                    Debug.WriteLine("Pairing confirmed by device {0}!", name);
                    await onPairingSucceeded();
                    return;
                }
                else if (wantsPair && pairStatus == PairStatus.RequestedByPeer)
                {
                    Debug.WriteLine("User on device {0} persists on pairing!",name);
                }
                else if (!wantsPair && pairStatus == PairStatus.Paired)
                {
                    Debug.WriteLine("User on device {0} removed us from trusted devices", name);
                    onRemoved();
                    return;
                }
                else
                {
                    Debug.WriteLine("Oops, something went wrong with device {0}!", name);
                }

            }
        }

        private void onLinkLost(object sender, DisconnectEventArgs disconnectEventArgs)
        {
            Debug.WriteLine("Link is lost: device {0}, reason {1}. It will be removed", name, disconnectEventArgs.status);
            m_links.Remove((Link) sender);
        }

        public async void Dispose()
        {
            if (m_needsSerializing)
            {
                await serialize();
            }
            store.Dispose();
        }

        private async Task serialize()
        {
            //Store device as paired
            var device = new TrustedDevice()
            {
                deviceName = this.name,
                deviceType = DeviceTypeConverter.deviceToString(m_deviceType)
            };
            //Serialize
            var s = JsonConvert.SerializeObject(device);

            //Save to file in storage

            using (var writer = new StreamWriter(store.OpenFile(Path.Combine(Constants.DevicesFolder, id), FileMode.Create)))
            {
                writer.Write(s);
                await writer.FlushAsync();
            }
        }
    }
}