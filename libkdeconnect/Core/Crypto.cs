﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Encodings;
using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.Pkcs;

namespace KdeConnectEarly.Core
{
//
   public class Crypto
    {
        /// <summary>
        /// get private key from stream
        /// </summary>
        /// <param name="reader">stream's StreamReader</param>
        /// <returns>key pair</returns>
        public static RsaPrivateCrtKeyParameters getPrivateKeyFromReader(TextReader reader)
        {
            AsymmetricCipherKeyPair o =
                (AsymmetricCipherKeyPair) new PemReader(reader).ReadObject();
           return o.Private as RsaPrivateCrtKeyParameters;
           
        }

       public static RsaKeyParameters getPublicKeyFromReader(TextReader reader)
       {
           return (RsaKeyParameters) (new PemReader(reader).ReadObject());
       }

       /// <summary>
       /// Creates RSA keys in PEM format
       /// </summary>
       /// <param name="writer"></param>
       /// <param name="parameters"></param>
       public static void writeKey(TextWriter writer, RsaKeyParameters parameters)
       {
           var pem = new PemWriter(writer);
           pem.WriteObject(parameters);
       }
       public static RsaKeyParameters getPublicKeyByPrivate(RsaPrivateCrtKeyParameters pub)
       {
           return new RsaKeyParameters(false, pub.Modulus, pub.PublicExponent);
       }

        /// <summary>
        /// get private key from PEM string
        /// </summary>
        /// <param name="pem"></param>
        /// <returns>public/private key pair or only public key</returns>
        public static RsaPrivateCrtKeyParameters getPrivateKeyFromString(string pem)
        {
            using (MemoryStream mem = new MemoryStream())
            {
                //write string to stream
                var writer = new StreamWriter(mem);
                writer.Write(pem);
                writer.Flush();
                mem.Position = 0;
                //read object from stream
                using (StreamReader reader = new StreamReader(mem))
                {
                    return getPrivateKeyFromReader(reader);
                }
            }
        }

    }
}
