﻿using System;
using System.Threading.Tasks;

namespace KdeConnectEarly.Core.Backends
{


    /// <summary>
    /// Event arguments for  connection state
    /// If NetworkPackage is null, connection is lost
    /// </summary>
    public class ConnectionEventArgs : EventArgs
    {
        /// <summary>
        /// this should be of type identity or null if connection is lost
        /// </summary>
        public NetworkPackage package { get; set; }

        public ConnectionEventArgs(NetworkPackage package)
        {
            this.package = package;
        }
    }

    /// <summary>
    /// Determines the method how network packets are delivered
    /// </summary>
    public abstract class LinkProvider
    {
        /// <summary>
        /// This is delegate for changing connection state
        ///  </summary>
        /// <param name="sender">Link that have been changed</param>
        /// <param name="e">Empty</param>
        public delegate void ConnectionStateHandler(Link sender, ConnectionEventArgs e);


        /// <summary>
        /// Emitted by Link when connection is accepted
        /// </summary>
        public event ConnectionStateHandler ConnectionChanged;

        /// <summary>
        /// Emits ConnectionChanged with identity package
        /// Call when provider links to new computer
        /// </summary>
        protected void onConnectionAccepted(Link sender, NetworkPackage identityPackage)
        {
            if (ConnectionChanged == null)
                return;
            ConnectionChanged(sender, new ConnectionEventArgs(identityPackage));
        }


        public abstract Task onStart();
        public abstract Task onStop();
        public abstract Task onNetworkChange();

        public abstract string name { get; }



    }
   
    
}