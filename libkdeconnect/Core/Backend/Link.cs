﻿using System;
using System.Diagnostics;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Core;
using Windows.Networking.Sockets;
using Org.BouncyCastle.Crypto.Parameters;

namespace KdeConnectEarly.Core.Backends
{

    /// <summary>
    /// For received packages
    /// </summary>
    /// <param name="package">Received package</param>
    /// <param name="sender">Object of type Link (for Device) or of type Device (for plugins)</param>
    public delegate void PackageReceivedHandler(object sender, NetworkPackage package);

    /// <summary>
    /// Represents basic device link. Reimplemented in LAN and Loopback
    /// TODO implement Loopback link in terms of  local sockets
    /// </summary>
    public abstract class Link : IDisposable
    {

        private SocketConsumer m_socket;

        protected SocketConsumer socket
        {
            get { return m_socket;}
            set
            {
                if (m_socket != null)
                {
                    m_socket.error -= onLost;
                    m_socket.messageReceived -= messageReceived;
                }
                m_socket = value;
                m_socket.error += onLost;
                m_socket.messageReceived += messageReceived;
            }
        }

        private void onLost(object sender, DisconnectEventArgs e)
        {
            if (linkLost != null)
                linkLost(sender, e);
        }
        public  ISocketConsumerInformation socketInfo { get { return m_socket.info;  } }

        protected readonly LinkProvider m_provider;
        public LinkProvider provider { get { return m_provider; } }

        protected readonly string m_deviceId;

        protected string deviceId { get { return m_deviceId;} }

        protected RsaPrivateCrtKeyParameters m_privateKey;


        public event PackageReceivedHandler packageReceived;
        public event EventHandler<DisconnectEventArgs> linkLost; 


        /// <summary>
        /// Call this from subclasses when package is received
        /// </summary>
        /// <param name="package"></param>
        protected void onPackageReceived(NetworkPackage package)
        {
            if (packageReceived == null) //no subscribers
                return;
            packageReceived(this, package);
        }

        protected Link(string deviceId, LinkProvider provider,  SocketConsumer socket)
        {
            if (deviceId == null
        || provider == null)
                throw new ArgumentNullException("", "None of constructor arguments could be NULL");
            this.m_provider = provider;
            this.m_deviceId = deviceId;
            this.socket = socket;
            this.m_privateKey = (RsaPrivateCrtKeyParameters)CoreApplication.Properties["privateKey"];
        }

        /// <summary>
        /// Obtain a encrypted package and decrypt it with my private key
        /// Fire PackageReceived then
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
         protected void messageReceived(object sender, MessageEventArgs e)
        {
            NetworkPackage package = NetworkPackage.unserialize(Encoding.UTF8.GetString(e.data, 0, e.data.Length));
            if (package.type == PackageTypes.PACKAGE_TYPE_ENCRYPTED)
            {
                NetworkPackage decrypted = NetworkPackage.decrypt(package, m_privateKey);
                if (decrypted.payloadTransferInfo != null)
                    throw new NotImplementedException("Packages with payload has not implemented yet.");
                onPackageReceived(decrypted);
            }
            else
            {
                if (package.payloadTransferInfo != null)
                    Debug.WriteLine("Ignore unencrypted package with payload");
                else
                {
                    onPackageReceived(package);
                }
            }
        }

        /// <summary>
        /// Send package over link
        /// </summary>
        /// <param name="package"></param>
        /// <returns></returns>
         public virtual async Task<bool> sendPackage(NetworkPackage package)
         {
             if (package.hasPayload())
                 throw new NotImplementedException();
             try
             {

                 return await socket.write(
                     Encoding.UTF8.GetBytes(
                         NetworkPackage.serialize(package)));
             }
             catch (Exception ex)
             {
                 Debug.WriteLine("Exception catched at LanLink::sendPackage: {0}", ex.Message);
                 return false;
             }
         }

        /// <summary>
        /// Send encrypted package over link
        /// </summary>
        /// <param name="package"></param>
        /// <param name="pubKey"></param>
        /// <returns></returns>
         public virtual async Task<bool> sendEncryptedPackage(NetworkPackage package, RsaKeyParameters pubKey)
         {
             if (package.hasPayload())
                 throw new NotImplementedException();
             try
             {
                 var encrypted = NetworkPackage.encrypt(package, pubKey);
                 return await socket.write(Encoding.UTF8.GetBytes(
                     NetworkPackage.serialize(encrypted)));
             }
             catch (Exception ex)
             {
                 Debug.WriteLine("Exception catched at LanLink::sendEncryptedPackage: {0}", ex.StackTrace);

                 return false;
             }
         }



         public void Dispose()
         {
             //call onLost to give opportunity to make changes for clients
             onLost(this, new DisconnectEventArgs(0));
             m_socket.Dispose();
         }
    }
}