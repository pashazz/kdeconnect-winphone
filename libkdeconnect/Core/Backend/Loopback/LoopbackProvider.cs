﻿using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace KdeConnectEarly.Core.Backends.Loopback
{
    public class LoopbackProvider : LinkProvider
    {
        private LoopbackLink deviceLink;
        private NetworkPackage identityPackage;

        public LoopbackProvider()
        {
            deviceLink = null;
            identityPackage = NetworkPackage.createIdentityPackage();
        }

        public override async Task onStart()
        {
          await  onNetworkChange();
        }

        public override async Task onStop()
        {
            await Task.Run(() =>
            {
                if (deviceLink != null)
                {
                    deviceLink.Dispose();
                    deviceLink = null;
                }
            });
        }

        public override async Task onNetworkChange()
        {
            if (deviceLink != null)
                await onStop();
            LoopbackLink newLink = new LoopbackLink(this);

            deviceLink = newLink;
            onConnectionAccepted(newLink, identityPackage);


        }

        public override string name
        {
            get { return "LoopbackProvider"; }
        }
    }
}