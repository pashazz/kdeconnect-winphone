﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using Windows.Networking.Sockets;
using Windows.Storage.Streams;
using Org.BouncyCastle.Bcpg;

namespace KdeConnectEarly.Core.Backends.Lan
{

    public class TcpSocketConsumerInformation : ISocketConsumerInformation, IDisposable
    {
        public TcpSocketConsumerInformation(StreamSocket socket)
        {
            m_socket = socket;
        }

    private StreamSocket m_socket;

        public string protocol
        {
            get { return "TCP"; }
        }

        public string hostname
        {
            get { return m_socket.Information.RemoteHostName.ToString(); }
        }

        public int port
        {
            get { return int.Parse(m_socket.Information.RemotePort); }
        }

        public void Dispose()
        {
            m_socket.Dispose();
        }
    }

    /// <summary>
    /// Wrapper around windows.networking.sockets for reading line by line
    /// </summary>
    public class TcpSocketConsumer : SocketConsumer, IDisposable
    {
    
        private StreamSocket m_socket;
        private DataWriter m_writer;
        private TcpSocketConsumerInformation m_info;
    
        public TcpSocketConsumer(StreamSocket socket)
        {
            m_socket = socket;
            m_writer = new DataWriter(socket.OutputStream);
            m_info = new TcpSocketConsumerInformation(socket);
            if (m_socket == null)
            {
                onError(new DisconnectEventArgs(0));
            }
            
        }


       

        /// <summary>
        /// Consume TCP stream and emit events when needed
        /// Call this method before writing to stream!
        /// </summary>
        public override async Task consumeStream()
        {
            DataReader reader = new DataReader(m_socket.InputStream);
            try
            {
                reader.InputStreamOptions = InputStreamOptions.Partial;
                List<byte> bytes = new List<byte>();
                while (true)
                {
                    //Read bytes until newline
                    if (await reader.LoadAsync(1) == 0)
                    {//connection closed by peer
                        return;
                    }
                    byte b = reader.ReadByte();
     
                    Debug.WriteLine(Encoding.UTF8.GetChars(new []{b})[0]);
                    bytes.Add(b);
                    //This will generate byte array w/length 1 and then convert it to UTF8 char array and then compare
                    //if (Encoding.UTF8.GetChars(new[] {b})[0] == '\n')
                    if (b == Constants.MessageSeparator) //Unicode for newline
                    {
                        //Create byte array
                        if (bytes.Count == 1)
                        {
                            bytes.Clear();
                            continue; //Do not want a single \n
                        }
                        onMessageReceived(new MessageEventArgs() {data = bytes.ToArray()});
                        bytes.Clear();
                    }
                    
                }
            }
            catch (Exception ex) //TODO catch only n/w exceptions
            {
                 onError(new DisconnectEventArgs(SocketError.GetStatus(ex.HResult)));
                 Debug.WriteLine("Exception catched at consumeStream: {0}", ex.StackTrace);
            }
            finally
            {
                reader.DetachStream();
            }
        }

    

        public override async Task<bool> write(byte[] data)
        {
            m_writer.WriteBytes(data);
            //Write end of the line
            m_writer.WriteByte(Constants.MessageSeparator);
            //Try to store the data
            try
            {
                await m_writer.StoreAsync();
                return true;
            }
            catch (Exception ex)
            {
                //Raise onDisconnected
                if (!Enum.IsDefined(typeof (SocketErrorStatus), ex.HResult))
                    throw; //Not a socket error hmmm
                Debug.WriteLine("Exception catched at SocketLineReader::write: {0}", ex.StackTrace);
                onError(new DisconnectEventArgs(SocketError.GetStatus(ex.HResult)));
                
                return false;
            }

            
        }

        public override ISocketConsumerInformation info
        {
            get { return m_info; }
        }

      

        public override void Dispose()
        {
            m_socket = null;
            m_writer.DetachStream();
            m_info.Dispose();
        }
    }
}