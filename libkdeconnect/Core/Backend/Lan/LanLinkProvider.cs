﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Media;
using Windows.Devices.HumanInterfaceDevice;
using Windows.Devices.Input;
using Windows.Networking;
using Windows.Networking.Connectivity;
using  Windows.Networking.Sockets;
namespace KdeConnectEarly.Core.Backends.Lan
{
    /// <summary>
    /// TODO deal with TCP port problem
    /// </summary>
    public class LanLinkProvider : LinkProvider, IDisposable
    {
        private static int port = 1714;
        private int tcpPort = port;
        

        /// <summary>
        /// Connected devices
        /// </summary>
        private Dictionary<string, LanLink> links;

        public enum ServerStatus
        {
            Stopped = 0, UDP = 1, TCP =2, Both=3
        }

        public ServerStatus status { get; set; }
        private DatagramSocket m_udpServer;
        private StreamSocketListener m_tcpServer;

        public LanLinkProvider()
        {
            //initialize UDP server
            m_udpServer = new DatagramSocket();
            m_udpServer.MessageReceived += udpMessageReceived;

  
            //initalize TCP server
            m_tcpServer = new StreamSocketListener();
            m_tcpServer.ConnectionReceived += newConnection;

            //Reflect network status changes
            NetworkInformation.NetworkStatusChanged += NetworkStatusHandler;
            status = ServerStatus.Stopped;
            
            links = new Dictionary<string, LanLink>();
        }

        /// <summary>
        /// TODO: Move this code into background task
        /// Check if network is not internet, but LAN and then call onNetworkChange
        /// We interested in 71: Wireless and 6: Ethernet
        /// </summary>
        /// <param name="sender"></param>
        private async void NetworkStatusHandler(object sender)
        {
            ConnectionProfile inet = NetworkInformation.GetInternetConnectionProfile();
            if (inet == null)
            {
                await onStop();
                return;
            }
            uint ianaCode = inet.NetworkAdapter.IanaInterfaceType;
            if (ianaCode == 71 || ianaCode == 6)
               await onNetworkChange();

        }


        private enum TcpConnectionStatus
        {
            Failed, PortInUse, Success
        }
        public override async Task onStart()
        {
            //TODO error handling HERE for TCP AND UDP
            //bind UDP server
            try
            {
                await m_udpServer.BindServiceNameAsync(port.ToString());
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Exception catched at LanLinkProvider::onStart: {0}", ex.StackTrace);
                return;
            }
            status += 1; //UDP

            //bind TCP server
            while (true)
            {
                TcpConnectionStatus tcpStatus = await tryBind();
                if (tcpStatus == TcpConnectionStatus.Failed)
                    return;
               if (tcpStatus == TcpConnectionStatus.Success)
                    break;
            }
            status += 2; //TCP
            await broadcast();
            
        }

    
        /// <summary>
        /// Broadcast my identity package
        /// </summary>
        private async Task broadcast()
        {
            NetworkPackage myIdentity = NetworkPackage.createIdentityPackage();
            
            myIdentity.set("tcpPort", tcpPort);
            using (
                var stream =
                    (await m_udpServer.GetOutputStreamAsync(new HostName("255.255.255.255"), port.ToString()))
                        .AsStreamForWrite())
            {
                using (var writer = new StreamWriter(stream))
                {
                    writer.NewLine = "\n";
                    await writer.WriteAsync(NetworkPackage.serialize(myIdentity));
                }
            }

        }
        /// <summary>
        /// Tries to bind TCP socket to tcpPort
        /// Can't make it recursive since we cannot await in catch block
        /// </summary>
        private async Task<TcpConnectionStatus> tryBind()
        {
            try
            {
                await m_tcpServer.BindServiceNameAsync(tcpPort.ToString());
                return TcpConnectionStatus.Success;
            }
            catch (Exception ex) //HRESULT 0x80072740 - only one usage of each port is permitted
            {
                if (SocketError.GetStatus(ex.HResult) == SocketErrorStatus.AddressAlreadyInUse)
                {
                    return TcpConnectionStatus.PortInUse;
                }
                else
                {
                    return TcpConnectionStatus.Failed;
                }
            }
        }

        /// <summary>
        /// When we receive a UDP message, this is usually a request for creating a TCP connection to the host
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private async void udpMessageReceived(DatagramSocket sender, DatagramSocketMessageReceivedEventArgs args)
        {
            NetworkPackage identity = await readIdentity(args.GetDataStream().AsStreamForRead());
            args.GetDataStream().Dispose();
            if (identity == null) //TODO logs
            {
                Debug.WriteLine("Error: cannot read UDP msg from {0}", args.RemoteAddress);
                return; //error occuried
            }
            NetworkPackage myIdentity = NetworkPackage.createIdentityPackage();
            string deviceId = identity.get("deviceId", "");
            if (deviceId.Equals(string.Empty))
            {
                Debug.WriteLine("Wrong identity package: no deviceId");
                return;
            }
            if (deviceId.Equals(myIdentity.get("deviceId", "hacker")))
            {
                return; //my identity
            }
            //get TCP port to connect to
            int connPort = identity.getInt("tcpPort", tcpPort);

            //Open a connection
            StreamSocket socket = new StreamSocket();
            try
            {
                configureSocket(socket);
                await socket.ConnectAsync(args.RemoteAddress, connPort.ToString());


            }
            catch (Exception ex)
            {
                //TODO try reverse connection
                Debug.WriteLine("Exception catched in udpDatagramReceived from {0}: TCP socket open error: {1}",
                    args.RemoteAddress, ex.Message);
                return;
            }
            //Initialize new Link
            bool ok = await initializeLink(deviceId, socket);
            if (!ok)
            {
                Debug.WriteLine("Failed to establish new Link for IP address {0}", args.RemoteAddress);
            }
            else
            {
                onConnectionAccepted(links[deviceId], identity);
            }
        }

        /// <summary>
        /// Creates new Link object for given TCP socket
        /// </summary>
        /// <param name="deviceId">device ID for creation</param>
        /// <param name="socket">given TCP socket</param>
        /// <returns>if link created successfully (and put into links collection)</returns>
        private async Task<bool> initializeLink(string deviceId, StreamSocket socket)
        {
            LanLink link = new LanLink(deviceId, this, socket);
            NetworkPackage identity = NetworkPackage.createIdentityPackage();
            //Send our identity
            if (!await link.sendPackage(identity))
                return false;
            //Dispose of existing link to this device
            LanLink oldLink;
            if (links.TryGetValue(deviceId, out oldLink))
            {
                oldLink.Dispose();
                oldLink.linkLost -= LinkOnLinkLost;
            }
            link.linkLost += LinkOnLinkLost;
            links.Add(deviceId, link);
            return true;
        }

        private void LinkOnLinkLost(object sender, DisconnectEventArgs disconnectEventArgs)
        {
            Debug.WriteLine("LanLinkProvider: Link to {0} has been lost", ((LanLink) sender).socketInfo.hostname);
            links.Remove(links.Keys.First(s => links[s].Equals((LanLink) sender)));
        }


        private async Task<NetworkPackage> readIdentity(Stream stream)
        {
            //open a stream to read from
            try
            {

                var reader = new StreamReader(stream);
                //await reading datagram
                var datagram = await reader.ReadToEndAsync();
                //Unserialize package
                NetworkPackage np = NetworkPackage.unserialize(datagram);
                //Return
                switch (np.type)
                {
                    case PackageTypes.PACKAGE_TYPE_IDENTITY:
                        return np;
                    default:
                        return null; //Error
                }
             
            }
            catch (Exception ex)
            {
                Debugger.Break();
                Debug.WriteLine("Exception catched in readIdentityDatagram({0}) : {1}", stream, ex.Message);
                return null;
            }

        }

        /// <summary>
        /// New connection for us: answer to UDP msg
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private async void newConnection(StreamSocketListener sender,
            StreamSocketListenerConnectionReceivedEventArgs args)
        {
            Debug.WriteLine("Opened a TCP connection to {0}", args.Socket.Information.RemoteHostName);
            //Obtain socket
            var socket = args.Socket;
            configureSocket(socket);
            //Read identity
            try
            {
                NetworkPackage innerIdentity = await readIdentity(socket.InputStream.AsStreamForRead());
                if (innerIdentity == null)
                {
                    Debug.WriteLine("Cannot read identity");
                    return;
                }
                string deviceId = innerIdentity.get("deviceId", "");
                Debug.Assert(!deviceId.Equals(string.Empty));
                if (await initializeLink(deviceId, socket))
                    onConnectionAccepted(links[deviceId], innerIdentity);
                else
                {
                    Debug.WriteLine("LanLinkProvider: Failed to establish link while TCP connection has been received");
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine("LanLinkProvider: exception catched: {0}", e.StackTrace);
            }

        }


        public override async Task onStop()
        {
            //Disconnect servers
            await Task.Run(() =>
            {
                m_udpServer.Dispose();
                m_tcpServer.Dispose();
            });


        }

        /// <summary>
        /// Sends a datagram to multicast addrress 255.255.255.255
        /// With my own identity package
        /// </summary>
        /// <returns></returns>
        public async override Task onNetworkChange()
        {

            await onStop();
            await onStart();
        }

        public override string name
        {
            get { return "LanLinkProvider"; }
        }

        public async void Dispose()
        {
            await onStop();
            m_udpServer.MessageReceived -= udpMessageReceived;
            m_tcpServer.ConnectionReceived -= newConnection;
            foreach (var link in links.Values)
            {
                link.Dispose();
                link.linkLost -= LinkOnLinkLost;
            }
            NetworkInformation.NetworkStatusChanged -= NetworkStatusHandler;
        }

        private void configureSocket(StreamSocket sock)
        {
            //Keepalive true
            sock.Control.KeepAlive = true;
            sock.Control.NoDelay = false;
            sock.Control.QualityOfService = SocketQualityOfService.Normal; //?
            

        }
    }
}