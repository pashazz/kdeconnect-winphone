﻿
using System;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Core;
using Windows.Networking.Sockets;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Crypto.Parameters;

namespace KdeConnectEarly.Core.Backends.Lan
{
    /// <summary>
    /// Represents device link over LAN network. Essential for device class
    /// <seealso cref="Device"/>
    /// </summary>
    public sealed class LanLink : Link
    {
        public LanLink(string deviceId, LanLinkProvider provider, StreamSocket socket)
            : base(deviceId, provider, new TcpSocketConsumer(socket))
        {
    
        }


   
    }
}