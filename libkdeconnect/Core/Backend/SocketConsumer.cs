﻿using System;
using System.Threading.Tasks;
using Windows.Networking.Sockets;

namespace KdeConnectEarly.Core.Backends
{

   public class DisconnectEventArgs : EventArgs
   {
     public DisconnectEventArgs(SocketErrorStatus status)
       {
                this.status = status;
      }
      public SocketErrorStatus status { get; set; }
   }

        public class MessageEventArgs : EventArgs
        {
            public byte[] data { get; set; }
        }

 public interface ISocketConsumerInformation
        {
            string protocol { get; }
            string hostname { get; }
            int port { get; }

        }


        public abstract class SocketConsumer : IDisposable
        {
            public event EventHandler<DisconnectEventArgs> error;
            public event EventHandler<MessageEventArgs> messageReceived;
            protected void onError(DisconnectEventArgs e)
            {
                if (error != null)
                    error(this, e);
            }

            protected void onMessageReceived(MessageEventArgs e)
            {
                if (messageReceived != null)
                    messageReceived(this, e);
            }

            /// <summary>
            /// Consume corresponding stream
            /// </summary>
            /// <returns></returns>
            public abstract Task consumeStream();

            /// <summary>
            /// Write data to corresponding stream
            /// </summary>
            /// <param name="data"></param>
            /// <returns></returns>
            public abstract Task<bool> write(byte[] data);

            public abstract ISocketConsumerInformation info { get; }

            public abstract void Dispose();

        } 
 }
