﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Linq;
using System.Threading;
using System.Windows.Controls.Primitives;
using Windows.Media.Capture.Core;

using kdeconnect.Threading;
using KdeConnectEarly.Core;
using Microsoft.Phone.Shell;

namespace kdeconnect.DataModel
{
 
    public class DevicesModel : INotifyPropertyChanged
    {
        private ObservableDictionary<string, Device> devices;
        private bool isDataLoaded;

        public UiObservableCollection<Device> untrusted { get; private set; }
        public UiObservableCollection<Device> trusted { get; private set; }

        public DevicesModel()
        {
            devices = App.devices;

            //To avoid threading issues, do not add/remove anything from this object
           // devices = ((App) Application).devices;
            devices.CollectionChanged += devicesCollectionChanged;
            untrusted = new UiObservableCollection<Device>();
            untrusted.CollectionChanged += untrustedCollectionChanged;
            trusted = new UiObservableCollection<Device>();
            //Load data

        }

        private void untrustedCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            Debug.WriteLine("Untrusted collection: action: {0} ", e.Action);
            if (e.Action == NotifyCollectionChangedAction.Add)
                Debug.WriteLine("Element added: {0}", untrusted[e.NewStartingIndex].name);
            else if (e.Action == NotifyCollectionChangedAction.Remove)
                Debug.WriteLine("Element removed: {0}", ((Device)e.OldItems[0]).name);

        }

        public void loadData()
        {
            if (isDataLoaded)
                return;
            foreach (var device in devices.Values)
            {
              onDeviceAdded(device);
            }
             isDataLoaded = true;
        }


        private void devicesCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
                foreach (var item in e.NewItems)
                {
                    var device = ((KeyValuePair<string,Device>) item).Value;
                    onDeviceAdded(device);
                }
            else
            {
                Debug.WriteLine("ViewModel: something unexpected: action {0}",e.Action);
            }
        }

        private void sendPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
            {
                UiDispatcher.RunOnUi(() =>
                PropertyChanged(this, new PropertyChangedEventArgs(propName)));
            }
        }

        private void onDeviceAdded(Device device)
        {
            device.packageReceived += onPackageReceived;
            device.pairingFailed += onPairingFailed;
            device.PropertyChanged += onPropertyChanged;


            if (device.pairStatus == Device.PairStatus.Paired)
            {
                trusted.Add(device);
                sendPropertyChanged("trusted");
                Debug.WriteLine("added new trusted device to the model");
            }
            else
            {
                untrusted.Add(device);
                sendPropertyChanged("untrusted");
                Debug.WriteLine("added new untrusted device to the model");
            }
        }

        private async void onPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            Device device = (Device) sender;
            if (e.PropertyName == "pairStatus")
            {//Pair status has changed
                if (trusted.Contains(device) && device.pairStatus == Device.PairStatus.NotPaired)
                {
                    trusted.Remove(device);
                    sendPropertyChanged("trusted");
                    untrusted.Add(device);
                    sendPropertyChanged("untrusted");
                }
                else if (untrusted.Contains(device) && device.pairStatus == Device.PairStatus.RequestedByPeer)
                {
                    //Temporary action: automatic accept
                    Debug.WriteLine("Accepting request as it wont hurt us yet");
                    await device.acceptPairing();
                }
                else if (untrusted.Contains(device) && device.pairStatus == Device.PairStatus.Requested)
                {
                    Debug.WriteLine("We requested connection");
                }
                else if (untrusted.Contains(device) && device.pairStatus == Device.PairStatus.Paired)
                {
                    untrusted.Remove(device);
                    sendPropertyChanged("untrusted");
                    trusted.Add(device);
                    sendPropertyChanged("trusted");

                    ShellToast toast = new ShellToast();
                    toast.Title = "KDE Connect";
                    toast.Content = string.Format("Device {0} is not trusted", device.name);
                    toast.Show();
                }
                else if (trusted.Contains(device))
                {
                    Debug.WriteLine("Device {0} changed its status to {1} while trusted. Something is possibly wrong",
                        device.name, device.pairStatus);
                }
            }
            else if (e.PropertyName == "reachable")
            {
                if (untrusted.Contains(device) && device.reachable == false)
                { //Do not show untursted non-reachable devices to the user
                    untrusted.Remove(device);
                    sendPropertyChanged("untrusted");

                }
                else if (device.reachable && !trusted.Contains(device) && device.pairStatus != Device.PairStatus.Paired)
                {//Show device that came up again to the user
                    untrusted.Add(device);
                    sendPropertyChanged("untrusted");

                }
            }
        }

        private void onPairingFailed(object sender, DevicePairingFailedEventArgs e)
        {
            Debug.WriteLine("Pairing was failed!");
        }

        private void onPackageReceived(object sender, NetworkPackage package)
        {//This method is temporary
            //Show shell toast notification
            if (package.type == "kdeconnect.ping")
            {
                ShellToast toast = new ShellToast();
                toast.Title = string.Format("KDE Connect: {0}", ((Device) sender).name);
                toast.Content = "Ping!";
                toast.Show();
            }
            else //Temportary
            {
                ShellToast toast = new ShellToast();
                toast.Title = string.Format("KDE Connect: {0}", ((Device)sender).name);
                toast.Content = package.type;
                toast.Show();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}