﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;


namespace kdeconnect.Threading
{
    /// <summary>
    /// Runs ObservableCollection's OnCollectionChanged within UI thread
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class UiObservableCollection<T> : ObservableCollection<T>
    {
        public override event NotifyCollectionChangedEventHandler CollectionChanged;

        protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            using (BlockReentrancy())
            {
                if (CollectionChanged != null)
                {
                    Delegate[] handlers = CollectionChanged.GetInvocationList();
                    foreach (NotifyCollectionChangedEventHandler handler in handlers)
                    {
                        UiDispatcher.RunOnUi(() => handler(this, e));
                    }
                }
            }
        }
    }
}