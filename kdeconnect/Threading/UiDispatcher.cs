﻿using System.Windows;

namespace kdeconnect.Threading
{
    public delegate void UiDispatcherDelegate();
    public class UiDispatcher
    {
        public static void RunOnUi(UiDispatcherDelegate func)
        {
            if (Deployment.Current.Dispatcher.CheckAccess())
            {
                func();
            }
            else
            {
                Deployment.Current.Dispatcher.BeginInvoke(func);
            }
        }
    }
}