﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Resources;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Markup;
using System.Windows.Navigation;
using Windows.ApplicationModel.Core;
using kdeconnect.DataModel;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using kdeconnect.Resources;
using KdeConnectEarly;
using KdeConnectEarly.Core;
using KdeConnectEarly.Core.Backends;
using KdeConnectEarly.Core.Backends.Lan;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Security;

namespace kdeconnect
{
    public partial class App : Application
    {
        public static LanLinkProvider provider { get; set; }
        public static ObservableDictionary<string, Device> devices = null;
        /// <summary>
        /// Обеспечивает быстрый доступ к корневому кадру приложения телефона.
        /// </summary>
        /// <returns>Корневой кадр приложения телефона.</returns>
        public static PhoneApplicationFrame RootFrame { get; private set; }

        private static DevicesModel m_model = null;
        public static DevicesModel viewModel
        {
            get
            {
                if (m_model == null)
                {
                    m_model = new DevicesModel();
                    
                }
                return m_model;
            }
        }
        /// <summary>
        /// Конструктор объекта приложения.
        /// </summary>
        public App()
        {
            // Глобальный обработчик неперехваченных исключений.
            UnhandledException += Application_UnhandledException;

            // Стандартная инициализация XAML
            InitializeComponent();

            // Инициализация телефона
            InitializePhoneApplication();

            // Инициализация отображения языка
            InitializeLanguage();

            // Отображение сведений о профиле графики во время отладки.
            if (Debugger.IsAttached)
            {
                // Отображение текущих счетчиков частоты смены кадров.
                Application.Current.Host.Settings.EnableFrameRateCounter = true;

                // Отображение областей приложения, перерисовываемых в каждом кадре.
                //Application.Current.Host.Settings.EnableRedrawRegions = true;

                // Включение режима визуализации анализа нерабочего кода,
                // для отображения областей страницы, переданных в GPU, с цветным наложением.
                //Application.Current.Host.Settings.EnableCacheVisualization = true;

                // Предотвратить выключение экрана в режиме отладчика путем отключения
                // определения состояния простоя приложения.
                // Внимание! Используйте только в режиме отладки. Приложение, в котором отключено обнаружение бездействия пользователя, будет продолжать работать
                // и потреблять энергию батареи, когда телефон не будет использоваться.
                PhoneApplicationService.Current.UserIdleDetectionMode = IdleDetectionMode.Disabled;
            }

        }

        // Код, который выполняется, если при активации контракта, такого как открытие файла или выбор файлов в окне сохранения, возвращается 
        // выбранный файл или другие возвращаемые значения
        private void Application_ContractActivated(object sender, Windows.ApplicationModel.Activation.IActivatedEventArgs e)
        {
        }

        // Код для выполнения при запуске приложения (например, из меню "Пуск")
        // Этот код не будет выполняться при повторной активации приложения
        private async void Application_Launching(object sender, LaunchingEventArgs e)
        {

            //Create folder
            using (var isoStore = IsolatedStorageFile.GetUserStoreForApplication())
            {
                isoStore.CreateDirectory(Constants.DevicesFolder);

                //Load RSA private key
                CoreApplication.Properties.Add("privateKey", loadPrivateKey(isoStore));

                //Initialize Providers
                Debug.WriteLine("Initialize Providers\n");
                if (provider != null)
                    return;
                provider = new LanLinkProvider();
                //Currently only one link provider      

            }
            Debug.WriteLine("\nInitialize Devices");

            await initializeDevices();

        }

        private async Task initializeDevices()
        {
            if (devices == null)
                devices = new ObservableDictionary<string, Device>();

            var store = IsolatedStorageFile.GetUserStoreForApplication();
            foreach (var file in store.GetFileNames(Constants.DevicesFolder + @"\*")) //Everything in Devices
            if (file.EndsWith(".pem"))
            {
                Debug.WriteLine("Loading id  from file: {0}", file);
                Device d = await Device.loadTrustedDevice(file);
                devices.Add(d.id, d);
            }
            //Listen to new devices
            provider.ConnectionChanged += linkConnection;
            await provider.onStart();
        }

        private void linkConnection(Link sender, ConnectionEventArgs e)
        {
            if (e.package == null)
            {//try to remove this Link from list of links
                Debug.WriteLine("Application: link reports death");
            }
            else
            {
                string id = e.package.get("deviceId", "");
                if (devices.ContainsKey(id))
                {
                    Debug.WriteLine("Application: Add known device: {0}", id);
                    var device = devices[id];
                    device.addLink(e.package,sender);
                }
                else
                {
                    Device d = new Device(e.package, sender);
                    devices.Add(d.id, d);
                    Debug.WriteLine("Application: Add new device: {0}", id);
                }
            }
        }

        

        // Код для выполнения при активации приложения (переводится в основной режим)
        // Этот код не будет выполняться при первом запуске приложения
        private void Application_Activated(object sender, ActivatedEventArgs e)
        {
        }

        // Код для выполнения при деактивации приложения (отправляется в фоновый режим)
        // Этот код не будет выполняться при закрытии приложения
        private void Application_Deactivated(object sender, DeactivatedEventArgs e)
        {
        }

        // Код для выполнения при закрытии приложения (например, при нажатии пользователем кнопки "Назад")
        // Этот код не будет выполняться при деактивации приложения
        private async void Application_Closing(object sender, ClosingEventArgs e)
        {
           await provider.onStop();
        }

        // Код для выполнения в случае ошибки навигации
        private void RootFrame_NavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            if (Debugger.IsAttached)
            {
                // Ошибка навигации; перейти в отладчик
                Debugger.Break();
            }
        }

        // Код для выполнения на необработанных исключениях
        private void Application_UnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e)
        {
            if (Debugger.IsAttached)
            {
                // Произошло необработанное исключение; перейти в отладчик
                Debug.WriteLine(e.ExceptionObject.StackTrace);
                Debugger.Break();
            }
        }

        #region Инициализация приложения телефона

        // Избегайте двойной инициализации
        private bool phoneApplicationInitialized = false;

        // Не добавляйте в этот метод дополнительный код
        private void InitializePhoneApplication()
        {
            if (phoneApplicationInitialized)
                return;

            // Создайте кадр, но не задавайте для него значение RootVisual; это позволит
            // экрану-заставке оставаться активным, пока приложение не будет готово для визуализации.
            RootFrame = new PhoneApplicationFrame();
            RootFrame.Navigated += CompleteInitializePhoneApplication;

            // Обработка сбоев навигации
            RootFrame.NavigationFailed += RootFrame_NavigationFailed;

            // Обработка запросов на сброс для очистки стека переходов назад
            RootFrame.Navigated += CheckForResetNavigation;

            // Обработка активации контракта, такого как открытие файла или выбор файлов в окне сохранения
            PhoneApplicationService.Current.ContractActivated += Application_ContractActivated;

            // Убедитесь, что инициализация не выполняется повторно
            phoneApplicationInitialized = true;
        }

        // Не добавляйте в этот метод дополнительный код
        private void CompleteInitializePhoneApplication(object sender, NavigationEventArgs e)
        {
            // Задайте корневой визуальный элемент для визуализации приложения
            if (RootVisual != RootFrame)
                RootVisual = RootFrame;

            // Удалите этот обработчик, т.к. он больше не нужен
            RootFrame.Navigated -= CompleteInitializePhoneApplication;
        }

        private void CheckForResetNavigation(object sender, NavigationEventArgs e)
        {
            // Если приложение получило навигацию "reset", необходимо проверить
            // при следующей навигации, чтобы проверить, нужно ли выполнять сброс стека
            if (e.NavigationMode == NavigationMode.Reset)
                RootFrame.Navigated += ClearBackStackAfterReset;
        }

        private void ClearBackStackAfterReset(object sender, NavigationEventArgs e)
        {
            // Отменить регистрацию события, чтобы оно больше не вызывалось
            RootFrame.Navigated -= ClearBackStackAfterReset;

            // Очистка стека только для "новых" навигаций (вперед) и навигаций "обновления"
            if (e.NavigationMode != NavigationMode.New && e.NavigationMode != NavigationMode.Refresh)
                return;

            // Очистка всего стека страницы для согласованности пользовательского интерфейса
            while (RootFrame.RemoveBackEntry() != null)
            {
                ; // ничего не делать
            }
        }

        #endregion

        // Инициализация шрифта приложения и направления текста, как определено в локализованных строках ресурсов.
        //
        // Чтобы убедиться, что шрифт приложения соответствует поддерживаемым языкам, а
        // FlowDirection для каждого из этих языков соответствует традиционному направлению, ResourceLanguage
        // и ResourceFlowDirection необходимо инициализировать в каждом RESX-файле, чтобы эти значения совпадали с
        // культурой файла. Пример:
        //
        // AppResources.es-ES.resx
        //    Значение ResourceLanguage должно равняться "es-ES"
        //    Значение ResourceFlowDirection должно равняться "LeftToRight"
        //
        // AppResources.ar-SA.resx
        //     Значение ResourceLanguage должно равняться "ar-SA"
        //     Значение ResourceFlowDirection должно равняться "RightToLeft"
        //
        // Дополнительные сведения о локализации приложений Windows Phone см. на странице http://go.microsoft.com/fwlink/?LinkId=262072.
        //
        private void InitializeLanguage()
        {
            try
            {
                // Задать шрифт в соответствии с отображаемым языком, определенным
                // строкой ресурса ResourceLanguage для каждого поддерживаемого языка.
                //
                // Откат к шрифту нейтрального языка, если отображаемый
                // язык телефона не поддерживается.
                //
                // Если возникла ошибка компилятора, ResourceLanguage отсутствует в
                // файл ресурсов.
                RootFrame.Language = XmlLanguage.GetLanguage(AppResources.ResourceLanguage);

                // Установка FlowDirection для всех элементов в корневом кадре на основании
                // строки ресурса ResourceFlowDirection для каждого
                // поддерживаемого языка.
                //
                // Если возникла ошибка компилятора, ResourceFlowDirection отсутствует в
                // файл ресурсов.
                FlowDirection flow = (FlowDirection)Enum.Parse(typeof(FlowDirection), AppResources.ResourceFlowDirection);
                RootFrame.FlowDirection = flow;
            }
            catch
            {
                // Если здесь перехвачено исключение, вероятнее всего это означает, что
                // для ResourceLangauge неправильно задан код поддерживаемого языка
                // или для ResourceFlowDirection задано значение, отличное от LeftToRight
                // или RightToLeft.

                if (Debugger.IsAttached)
                {
                    Debugger.Break();
                }

                throw;
            }
        }
        private RsaPrivateCrtKeyParameters loadPrivateKey(IsolatedStorageFile store)
        {
            if (store.FileExists("key.pem"))
            {
                RsaPrivateCrtKeyParameters parameters;
                using (var file = store.OpenFile("key.pem", FileMode.Open))
                 using (var stream = new StreamReader(file))
                   {
                     Debug.WriteLine("Load RSA private key (exists)");
                       parameters = Crypto.getPrivateKeyFromReader(stream);

                   }
                    return parameters;

                }
              
            return generateKey(store);
        }

        private RsaPrivateCrtKeyParameters generateKey(IsolatedStorageFile store)
        {
            RsaKeyPairGenerator r = new RsaKeyPairGenerator();
            r.Init(new KeyGenerationParameters(new SecureRandom(), 1024));
            AsymmetricCipherKeyPair keys = r.GenerateKeyPair();
            var pKey = (RsaPrivateCrtKeyParameters)keys.Private;
            //Write private key to file
            using (var file = store.OpenFile("key.pem",FileMode.Create))
            {
                using (var stream = new StreamWriter(file))
                {
                        Debug.WriteLine("Write new 1024-bit RSA private key");
                        Crypto.writeKey(stream, pKey);

                }
            }
            return pKey;
        }

    }
}