﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using kdeconnect.DataModel;
using KdeConnectEarly.Core;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace kdeconnect
{
    public partial class MainPage : PhoneApplicationPage
    {
        public MainPage()
        {
            InitializeComponent();
            DataContext = App.viewModel;

        }
        public ObservableCollection<Device> devices { get; set; }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            App.viewModel.loadData();
        }
    }
}